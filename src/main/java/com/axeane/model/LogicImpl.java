package com.axeane.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogicImpl implements Logic {

  //TASK 4
  public boolean capitalPeriod(String sentence) {
    Pattern pattern = Pattern.compile("^[A-Z]+[a-zA-Z,;'\"\\s]+[.]$");
    Matcher matcher = pattern.matcher(sentence);
    return matcher.matches();
  }

  //TASK 5
  public String[] splitByTheAndYou(String sentence) {
    return sentence.split("the | you");
  }

  //TASK 6
  public String replaceTheVowels(String sentence) {
    return sentence.replaceAll("[aeyuio]", "_");
  }
}
