package com.axeane.model;

public interface Logic {

  /**
   * TASK 4
   * Checking the sentence if it begins with a capital letter
   * and ends with a period.
   *
   * @param sentence
   * @return true or false
   */
  boolean capitalPeriod(String sentence);

  /**
   * TASK 5
   * Splitting of string on the words "the" or "you".
   *
   * @param sentence
   * @return array of strings
   */
  String[] splitByTheAndYou(String sentence);

  /**
   * TASK 6
   * Replacing all the vowels in text with underscores.
   *
   * @param sentence
   * @return replacement string
   */
  String replaceTheVowels(String sentence);
}
