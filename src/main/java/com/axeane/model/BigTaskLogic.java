package com.axeane.model;

import java.util.List;

public interface BigTaskLogic {

  /**
   * BIGTASK initialization 01
   * Get strings from file and adding them to list.
   *
   * @return list with strings
   */
  List<String> readFromFileList();

  /**
   * BIGTASK initialization 02
   * Clean text from spaces and tabulations
   *
   * @param list
   * @return new list with clean strings
   */
  List<String> cleanText(List<String> list);
}
