package com.axeane.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BigTaskLogicImpl implements BigTaskLogic {

  public List<String> readFromFileList() {
    List<String> text = new ArrayList<>();
    try {
      text = Files.lines(Paths.get("text.txt"), StandardCharsets.UTF_8)
          .collect(Collectors.toList());
    } catch (IOException e) {
      e.getMessage();
    }
    return text;
  }

  public List<String> cleanText(List<String> list) {
    List<String> newList = new ArrayList();
    for(String str : list) {
      newList.add(str.replaceAll("\\s+ | \\t+", " "));
    }
    return newList;
  }
}
