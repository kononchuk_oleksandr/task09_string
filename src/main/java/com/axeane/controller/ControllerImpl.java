package com.axeane.controller;

import com.axeane.model.Logic;
import com.axeane.model.LogicImpl;
import java.util.Arrays;

public class ControllerImpl implements Controller {
  Logic logic = new LogicImpl();

  @Override
  public boolean capitalPeriod(String sentence) {
    return logic.capitalPeriod(sentence);
  }

  @Override
  public String splitByTheAndYou(String sentence) {
    return Arrays.toString(logic.splitByTheAndYou(sentence));
  }

  @Override
  public String replaceTheVowels(String sentence) {
    return logic.replaceTheVowels(sentence);
  }
}
