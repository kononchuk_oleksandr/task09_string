package com.axeane.controller;

public interface Controller {
  boolean capitalPeriod(String sentence);
  String splitByTheAndYou(String sentence);
  String replaceTheVowels(String sentence);
}
