package com.axeane.view;

import com.axeane.controller.Controller;
import com.axeane.controller.ControllerImpl;
import com.axeane.model.BackException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NewView {
  private static Logger logger = LogManager.getLogger(NewView.class);
  private Language language = new Language();
  private ResourceBundle rb = language.getResourceBundle(new Locale("", ""));
  private Controller controller = new ControllerImpl();
  private Scanner scanner = new Scanner(System.in);
  private Map<String, String> menuLanguage;
  private Map<String, String> menu;
  private Map<String, Printable> menuImpl;

  public NewView() {
    menuLanguage = new LinkedHashMap<>();
    menuLanguage.put("1", "1 - English");
    menuLanguage.put("2", "2 - Ukrainian");

    setMenuLanguage();

    menu = new LinkedHashMap<>();
    menu.put("1", "1 - " + rb.getString("strTask4"));
    menu.put("2", "2 - " + rb.getString("strTask5"));
    menu.put("3", "3 - " + rb.getString("strTask6"));
    menu.put("4", "4 - " + rb.getString("strBigTask"));
    menu.put("0", "0 - " + rb.getString("strExit"));

    menuImpl = new LinkedHashMap<>();
    menuImpl.put("1", this::capitalPeriod);
    menuImpl.put("2", this::splitByTheAndYou);
    menuImpl.put("3", this::replaceTheVowels);
  }

  private void setMenuLanguage() {
    menuLanguage.values().forEach(str -> logger.info(str));
    String key = scanner.nextLine();
    if (key.equals("2")) {
      rb = language.getResourceBundle(language.UKRAINIAN);
      logger.info(rb.getString("setLg"));
    } else {
      logger.info(rb.getString("setLg"));
    }
  }

  private void menu() {
    menu.values().forEach(str -> logger.info(str));
  }


  //TASK 5
  private void capitalPeriod() {
    logger.info(rb.getString("enter"));
    logger.info(rb.getString("result") +
        controller.capitalPeriod(scanner.nextLine()));
    throw new BackException();
  }

  //TASK6
  private void splitByTheAndYou() {
    logger.info(rb.getString("enter"));
    logger.info(rb.getString("result") +
        controller.splitByTheAndYou(scanner.nextLine()));
    throw new BackException();
  }

  //TASK 6
  private void replaceTheVowels() {
    logger.info(rb.getString("enter"));
    logger.info(rb.getString("result") +
        controller.replaceTheVowels(scanner.nextLine()));
    throw new BackException();
  }

  public void start() {
    String key = null;
    do {
      menu();
      logger.info(rb.getString("enter"));
      key = scanner.nextLine();
      try {
        menuImpl.get(key).print();
      } catch (Exception e) {
        e.getMessage();
      }
    } while (!key.equals("0"));
  }
}
