package com.axeane.view;

import java.util.Locale;
import java.util.ResourceBundle;

public class Language {
  public final Locale UKRAINIAN = new Locale("uk", "UA");

  public ResourceBundle getResourceBundle(Locale locale) {
    return ResourceBundle.getBundle("text", locale);
  }
}
